highlight clear
if exists("syntax_on")
    syntax reset
endif

set background=dark
let g:colors_name="okay"

let black =       '#1f262e'
let gray_darker = '#242d36'
let gray_dark =   '#33404d'
let grayer =      '#435465'
let gray =        '#566b81'
let gray_light =  '#bcc7d2'
let white =       '#eaedf1'
let red =         '#f14159' "old #f25469
let pink =        '#f359b0' "old #f359b0
let purple =      '#af7af5' "old #c071f4
let blue =        '#4199f1' "old #59a6f3
let cyan =        '#41dff1' "old #33ebff
let green =       '#66ffa8' "old #33ff8b
let yellow =      '#f8eea0' "old #fdef88

" hi TextType guifg=#hex guibg=#hex gui=bold/undercurl
:exe 'hi Comment         guifg='.gray.'         guibg=NONE             gui=NONE'
:exe 'hi Normal          guifg='.white.'        guibg='.black.'        gui=NONE'
:exe 'hi NormalNC        guifg='.white.'        guibg='.black.'        gui=NONE'
:exe 'hi NormalFloat     guifg='.white.'        guibg='.black.'        gui=NONE'
:exe 'hi Search          guifg='.black.'        guibg='.green.'        gui=NONE'
:exe 'hi IncSearch       guifg='.black.'        guibg='.green.'        gui=NONE'
:exe 'hi Substitute      guifg='.black.'        guibg='.green.'        gui=bold' 
:exe 'hi String          guifg='.green.'        guibg=NONE             gui=NONE'
:exe 'hi Constant        guifg='.yellow.'       guibg=NONE             gui=NONE'
:exe 'hi Character       guifg='.cyan.'         guibg=NONE             gui=NONE'
:exe 'hi Number          guifg='.cyan.'         guibg=NONE             gui=NONE'
:exe 'hi Float           guifg='.cyan.'         guibg=NONE             gui=NONE'
:exe 'hi Boolean         guifg='.purple.'       guibg=NONE             gui=NONE'
:exe 'hi Identifier      guifg='.white.'        guibg=NONE             gui=bold'
:exe 'hi Function        guifg='.blue.'         guibg=NONE             gui=bold'
:exe 'hi Statement       guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Operator        guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Keyword         guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Conditional     guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Repeat          guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Label           guifg='.purple.'       guibg=NONE             gui=bold'
:exe 'hi Exception       guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi PreProc         guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Include         guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Define          guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi Macro           guifg='.pink.'         guibg=NONE             gui=bold'
:exe 'hi PreCondit       guifg='.pink.'         guibg=NONE             gui=NONE'
:exe 'hi Type            guifg='.purple.'       guibg=NONE             gui=bold'
:exe 'hi StorageClass    guifg='.purple.'       guibg=NONE             gui=bold'
:exe 'hi Structure       guifg='.purple.'       guibg=NONE             gui=bold'
:exe 'hi Typedef         guifg='.purple.'       guibg=NONE             gui=bold'
:exe 'hi Special         guifg='.green.'        guibg=NONE             gui=bold'
:exe 'hi SpecialCha      guifg='.white.'        guibg=NONE             gui=NONE'
:exe 'hi Tag             guifg='.red.'          guibg=NONE             gui=NONE'
:exe 'hi Delimiter       guifg='.green.'        guibg=NONE             gui=NONE'
:exe 'hi SpecialComment  guifg='.white.'        guibg=NONE             gui=NONE'
:exe 'hi Debug           guifg='.white.'        guibg=NONE             gui=NONE'
:exe 'hi Underlined      guifg='.blue.'         guibg=NONE             gui=underline'
:exe 'hi Ignore          guifg='.gray.'         guibg=NONE             gui=NONE'
:exe 'hi Error           guifg='.white.'        guibg='.red.'          gui=bold'
:exe 'hi ErrorMsg        guifg='.white.'        guibg='.red.'          gui=bold'
:exe 'hi WarningMsg      guifg='.black.'        guibg='.yellow.'       gui=bold'
:exe 'hi Todo            guifg='.cyan.'         guibg='.gray_dark.'    gui=bold'
:exe 'hi ColorColumn     guifg=NONE             guibg='.black.'        gui=NONE'
:exe 'hi Cursor          guifg='.black.'        guibg='.white.'        gui=NONE'
:exe 'hi CursorIM        guifg='.black.'        guibg='.gray_light.'   gui=NONE'
:exe 'hi TermCursor      guifg='.black.'        guibg='.white.'        gui=NONE'
:exe 'hi TermCursorNC    guifg='.black.'        guibg='.gray.'         gui=NONE'
:exe 'hi CursorColumn    guifg='.black.'        guibg='.white.'        gui=NONE'
:exe 'hi CursorLine      guifg='.black.'        guibg='.white.'        gui=NONE'
:exe 'hi Directory       guifg='.gray_light.'   guibg=NONE             gui=NONE'
:exe 'hi DiffAdd         guifg='.white.'        guibg='.green.'        gui=NONE'
:exe 'hi DiffChange      guifg='.white.'        guibg=NONE             gui=NONE'
:exe 'hi DiffDelete      guifg='.white.'        guibg='.red.'          gui=NONE'
:exe 'hi DiffText        guifg='.yellow.'       guibg=NONE             gui=NONE'
:exe 'hi EndOfBuffer     guifg='.gray.'         guibg=NONE             gui=bold'
:exe 'hi NonText         guifg='.gray.'         guibg=NONE             gui=bold'
:exe 'hi VertSplit       guifg='.gray.'         guibg=NONE             gui=NONE'
:exe 'hi Folded          guifg='.gray.'         guibg=NONE             gui=bold'
:exe 'hi FoldColumn      guifg='.gray.'         guibg=NONE             gui=bold'
:exe 'hi LineNr          guifg='.gray.'         guibg='.gray_dark.'    gui=NONE'
:exe 'hi CursorLineNr    guifg='.purple.'       guibg='.black.'        gui=bold'
:exe 'hi MatchParen      guifg='.pink.'         guibg=NONE             gui=NONE'
:exe 'hi ModeMsg         guifg='.white.'        guibg=NONE             gui=bold'
:exe 'hi MsgArea         guifg='.gray_light.'   guibg=NONE             gui=NONE'
:exe 'hi MoreMsg         guifg='.yellow.'       guibg=NONE             gui=NONE'
:exe 'hi Pmenu           guifg='.white.'        guibg='.gray_dark.'    gui=NONE'
:exe 'hi PmenuSel        guifg='.white.'        guibg='.gray.'         gui=bold'
:exe 'hi PmenuSbar       guifg=NONE             guibg='.gray_dark.'    gui=NONE'
:exe 'hi PmenuThumb      guifg=NONE             guibg='.gray.'         gui=NONE'
:exe 'hi SpellBad        guifg='.red.'          guibg=NONE             gui=undercurl'
:exe 'hi SpellLocal      guifg='.purple.'       guibg=NONE             gui=undercurl'
:exe 'hi StatusLine      guifg='.gray_light.'   guibg='.gray_dark.'    gui=NONE'
:exe 'hi StatusLineNC    guifg='.gray_light.'   guibg='.gray_dark.'    gui=NONE'
:exe 'hi TabLine         guifg='.gray_light.'   guibg='.gray_dark.'    gui=NONE'
:exe 'hi TabLineFill     guifg='.gray_light.'   guibg='.gray_dark.'    gui=NONE'
:exe 'hi TabLineSel      guifg='.gray_light.'   guibg='.gray_dark.'    gui=NONE'
:exe 'hi Visual          guifg=NONE             guibg='.gray_dark.'    gui=NONE'
:exe 'hi VisualNOS       guifg=NONE             guibg='.gray_dark.'    gui=NONE'
:exe 'hi WildMenu        guifg=NONE             guibg='.gray.'         gui=NONE'
:exe 'hi SignColumn      guifg='.gray_light.'   guibg='.black.'        gui=NONE'
:exe 'hi Title           guifg='.yellow.'       guibg=NONE             gui=bold'

" Markdown
:exe 'hi markdownBold              guifg='.white.'        guibg=NONE gui=bold'
:exe 'hi markdownBoldDelimiter     guifg='.gray_light.'   guibg=NONE gui=bold'
:exe 'hi markdownItalic            guifg='.white.'        guibg=NONE gui=italic'
:exe 'hi markdownItalicDelimiter   guifg='.gray_light.'   guibg=NONE gui=italic'
:exe 'hi markdownLinkText          guifg='.gray_light.'   guibg=NONE gui=bold'
:exe 'hi markdownLinkTextDelimiter guifg='.gray.'         guibg=NONE gui=bold'
:exe 'hi markdownLinkDelimiter     guifg='.gray.'         guibg=NONE gui=bold'
:exe 'hi markdownUrl               guifg='.blue.'         guibg=NONE gui=bold'
:exe 'hi markdownCode              guifg='.gray_light.'   guibg=NONE gui=NONE'
:exe 'hi markdownCodeDelimiter     guifg='.gray_light.'   guibg=NONE gui=NONE'
:exe 'hi markdownH1Delimiter       guifg='.yellow.'       guibg=NONE gui=bold'
:exe 'hi markdownH2Delimiter       guifg='.yellow.'       guibg=NONE gui=bold'
:exe 'hi markdownH3Delimiter       guifg='.yellow.'       guibg=NONE gui=bold'
:exe 'hi markdownH4Delimiter       guifg='.yellow.'       guibg=NONE gui=bold'
:exe 'hi markdownH5Delimiter       guifg='.yellow.'       guibg=NONE gui=bold'
:exe 'hi markdownH6Delimiter       guifg='.yellow.'       guibg=NONE gui=bold'

" JavaScript
:exe 'hi javaScriptBoolean    guifg='.purple.'   guibg=NONE  gui=NONE'
:exe 'hi jsObjectKey          guifg='.purple.'   guibg=NONE  gui=NONE'
:exe 'hi jsStorageClass       guifg='.pink.'     guibg=NONE  gui=bold'
:exe 'hi javaScriptIdentifier guifg='.pink.'     guibg=NONE  gui=bold'
:exe 'hi javaScriptBraces     guifg='.white.'    guibg=NONE  gui=NONE'
:exe 'hi javaScriptValue      guifg='.cyan.'     guibg=NONE  gui=NONE'
:exe 'hi javaScript           guifg='.white.'    guibg=NONE  gui=NONE'

" TypeScript
:exe 'hi typescriptVariable guifg='.pink.'  guibg=NONE gui=bold'
:exe 'hi typescriptBraces   guifg='.white.' guibg=NONE gui=NONE'

" Crystal
:exe 'hi crystalInterpolationDelim    guifg='.green.'  guibg=NONE gui=bold'
:exe 'hi ecrystalDelimiter            guifg='.yellow.' guibg=NONE gui=bold'

" CSS
:exe 'hi cssProp          guifg='.purple.'   guibg=NONE gui=bold'
:exe 'hi cssAttrComma     guifg='.white.'    guibg=NONE gui=NONE'
:exe 'hi cssClassName     guifg='.pink.'     guibg=NONE gui=bold'
:exe 'hi cssClassNameDot  guifg='.pink.'     guibg=NONE gui=bold'
:exe 'hi cssPseudoClassID guifg='.yellow.'   guibg=NONE gui=NONE'
:exe 'hi cssColor         guifg='.white.'    guibg=NONE gui=NONE'
:exe 'hi cssAttr          guifg='.white.'    guibg=NONE gui=NONE'
:exe 'hi cssIdentifier    guifg='.pink.'     guibg=NONE gui=bold'
:exe 'hi cssImportant     guifg='.red.'      guibg=NONE gui=bold'
:exe 'hi cssFunctionName  guifg='.blue.'     guibg=NONE gui=NONE'
:exe 'hi cssCommonAttr    guifg='.yellow.'   guibg=NONE gui=NONE'
:exe 'hi cssCustomProp    guifg='.cyan.'     guibg=NONE gui=NONE'
:exe 'hi cssBraces        guifg='.white.'    guibg=NONE gui=NONE'
:exe 'hi cssFunctionComma guifg='.white.'    guibg=NONE gui=NONE'

" HTML
:exe 'hi htmlTag         guifg='.blue.'     guibg=NONE gui=bold'
:exe 'hi htmlEndTag      guifg='.blue.'     guibg=NONE gui=bold'
:exe 'hi htmlTagName     guifg='.pink.'     guibg=NONE gui=bold'
:exe 'hi htmlArg         guifg='.purple.'   guibg=NONE gui=bold'
:exe 'hi htmlSpecialChar guifg='.cyan.'     guibg=NONE gui=NONE'
:exe 'hi htmlBold        guifg='.white.'    guibg=NONE gui=bold'

" Elixir
:exe 'hi elixirAtom                   guifg='.blue.'   guibg=NONE gui=NONE'
:exe 'hi elixirSigilDelimiter         guifg='.yellow.' guibg=NONE gui=NONE'
:exe 'hi elixirInterpolationDelimiter guifg='.yellow.' guibg=NONE gui=NONE'
:exe 'hi elixirDocTest                guifg='.cyan.'   guibg=NONE gui=NONE'
:exe 'hi elixirDocStringDelimiter     guifg='.gray.'   guibg=NONE gui=NONE'
:exe 'hi eelixirDelimiter             guifg='.yellow.' guibg=NONE gui=bold'

" Rust
:exe 'hi rustSigil        guifg='.yellow.'   guibg=NONE gui=NONE'
:exe 'hi rustStorage      guifg='.pink.'     guibg=NONE gui=bold'
:exe 'hi rustMacro        guifg='.blue.'     guibg=NONE gui=bold'
:exe 'hi rustModPath      guifg='.purple.'   guibg=NONE gui=bold'
:exe 'hi rustModPathSep   guifg='.white.'    guibg=NONE gui=NONE'
:exe 'hi rustLifetime     guifg='.pink.'     guibg=NONE gui=NONE'
:exe 'hi rustQuestionMark guifg='.pink.'     guibg=NONE gui=NONE'
:exe 'hi rustAttribute    guifg='.pink.'     guibg=NONE gui=bold'

" C
:exe 'hi cSpecial guifg='.green.'    guibg=NONE gui=bold'
:exe 'hi cLabel   guifg='.pink.'     guibg=NONE gui=NONE'

" Jinja
:exe 'hi jinjaString   guifg='.green.'  guibg=NONE gui=NONE'
:exe 'hi jinjaTagDelim guifg='.yellow.' guibg=NONE gui=bold'
:exe 'hi jinjaVarDelim guifg='.yellow.' guibg=NONE gui=bold'

" JSON
:exe 'hi jsonBraces  guifg='.pink.'     guibg=NONE gui=bold'
:exe 'hi jsonKeyword guifg='.purple.'   guibg=NONE gui=bold'

" Sailfish
:exe 'hi sailfishTag guifg='.yellow.' guibg=NONE gui=bold'

" Vim
:exe 'hi vimIsCommand guifg='.blue.'  guibg=NONE gui=bold'
:exe 'hi vimCommand   guifg='.blue.'  guibg=NONE gui=bold'
:exe 'hi vimCmdSep    guifg='.blue.'  guibg=NONE gui=bold'
:exe 'hi vimSetEqual  guifg='.pink.'  guibg=NONE gui=bold'
:exe 'hi vimSet       guifg='.pink.'  guibg=NONE gui=bold'
:exe 'hi vimVar       guifg='.white.' guibg=NONE gui=NONE'
:exe 'hi vimOption    guifg='.white.' guibg=NONE gui=NONE'
:exe 'hi vimParenSep  guifg='.white.' guibg=NONE gui=NONE'

" Vim Auto Indent
:exe 'hi IndentGuidesOdd  guibg='.black.'       gui=NONE'
:exe 'hi IndentGuidesEven guibg='.gray_darker.' gui=NONE'

" Visual Basic for Applications
:exe 'hi vbMethods  guifg='.blue.' guibg=NONE gui=bold'
:exe 'hi vbFunction guifg='.blue.' guibg=NONE gui=bold'

" XML
:exe 'hi xmlTag             guifg='.pink.' guibg=none gui=bold'
:exe 'hi xmlTagName         guifg='.pink.' guibg=none gui=bold'
:exe 'hi xmlEndTag          guifg='.pink.' guibg=none gui=bold'
:exe 'hi xmlProcessingDelim guifg='.pink.' guibg=none gui=bold'
:exe 'hi xmlDocTypeDecl     guifg='.pink.' guibg=none gui=bold'

" YAML
:exe 'hi yamlBlockMappingKey guifg='.cyan.' guibg=none gui=bold'

" Lightline
let s:black = [ black, 235 ]
let s:gray = [ gray_dark, 236 ]
let s:white = [ white, 250 ]
let s:blue = [ blue, 67 ] 
let s:green = [ green, 71 ] 
let s:purple = [ purple, 104 ]
let s:red = [ red, 204 ]
let s:yellow = [ yellow, 222 ]

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}
let s:p.normal.left = [ [ s:black, s:purple ], [ s:purple, s:gray ] ]
let s:p.normal.right = [ [ s:black, s:purple ], [ s:purple, s:gray ] ]
let s:p.inactive.left =  [ [ s:black, s:blue ], [ s:blue, s:gray ] ]
let s:p.inactive.right = [ [ s:black, s:blue ], [ s:blue, s:gray ] ]
let s:p.insert.left = [ [ s:black, s:green ], [ s:green, s:gray ] ] 
let s:p.insert.right = [ [ s:black, s:green ], [ s:green, s:gray ] ]
let s:p.replace.left = [ [ s:white, s:red ], [ s:red, s:gray ] ]
let s:p.replace.right = [ [ s:black, s:red ], [ s:red, s:gray ] ]
let s:p.visual.left = [ [ s:black, s:yellow ], [ s:yellow, s:gray ] ]
let s:p.visual.right = [ [ s:black, s:yellow ], [ s:yellow, s:gray ] ]
let s:p.normal.middle = [ [ s:white, s:gray ] ]
let s:p.inactive.middle = [ [ s:white, s:gray ] ]
let s:p.tabline.left = [ [ s:blue, s:gray ] ]
let s:p.tabline.tabsel = [ [ s:black, s:blue ] ]
let s:p.tabline.middle = [ [ s:blue, s:gray ] ]
let s:p.tabline.right = [ [ s:black, s:blue ] ]
let s:p.normal.error = [ [ s:red, s:black ] ]
let s:p.normal.warning = [ [ s:yellow, s:black ] ]

let g:lightline#colorscheme#okay#palette = lightline#colorscheme#flatten(s:p)
